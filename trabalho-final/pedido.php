<?php
    include("includes/cabecalho.php")
?>

<div class="container-fluid">
	<div class="row">
		<div class="col-3">
			<div class="row">
				<div class="col-12">
					<button class="btn btn-primary btn-block btn-lg btn-pedido" type="button"
					onclick="pedido.alterarJanelaPedido('#tamanho')">Tamanho</button>
				</div>
				<div class="col-12">
					<button class="btn btn-primary btn-block btn-lg btn-pedido" type="button"
					onclick="pedido.alterarJanelaPedido('#sabores')">Sabores</button>
				</div>
				<div class="col-12">
					<button class="btn btn-primary btn-block btn-lg btn-pedido" type="button"
					onclick="pedido.alterarJanelaPedido('#tipo-massa')">Massa</button>
				</div>
				<div class="col-12">
					<button class="btn btn-primary btn-block btn-lg btn-pedido" type="button"
					onclick="pedido.alterarJanelaPedido('#borda')">Borda</button>
				</div>

			</div>
		</div>

		<div class="col-9">
			<div class="painel-pedido">
				<div id="tamanho" class="none">
					<div class="container">
						<h4>Escolha o tamanho da sua pizza</h4>	

					<div class="custom-control custom-radio">
						  <input type="radio" id="pequena" name="tamanho" class="custom-control-input" 
						  onchange="pedido.escolhaRadios(event)" value="Pequena">
						  <label class="custom-control-label" for="pequena" >Pequena</label>
					</div>
					<div class="custom-control custom-radio">
						  <input type="radio" id="media" name="tamanho" class="custom-control-input" value="Média"
						   onchange="pedido.escolhaRadios(event)">
						  <label class="custom-control-label" for="media">Média</label>
					</div>
					<div class="custom-control custom-radio">
						  <input type="radio" id="grande" name="tamanho" class="custom-control-input" value="Grande" 
						  onchange="pedido.escolhaRadios(event)">
						  <label class="custom-control-label" for="grande">Grande</label>
					</div>

					<button type="button" class="btn btn-primary float-right" onclick="pedido.proximaTela('#sabores', '#tamanho')">
						Proximo
					</button>
					</div>
				</div>

				<div id="sabores" class="none">

					
				</div>

				<div id="tipo-massa" class="none">
					<h4>Escolha a Massa</h4>	

					<div class="custom-control custom-radio">
						  <input type="radio" id="integral" name="massa" class="custom-control-input" 
						  onchange="pedido.escolhaRadios(event)" value="Integral">
						  <label class="custom-control-label" for="integral">Integral</label>
					</div>
					<div class="custom-control custom-radio">
						  <input type="radio" id="normal" name="massa" class="custom-control-input" 
						  onchange="pedido.escolhaRadios(event)" value="Normal">
						  <label class="custom-control-label" for="normal">Normal</label>
					</div>

					<button type="button" class="btn btn-primary float-right" onclick="pedido.proximaTela('#borda', '#tipo-massa')">
						Proximo
					</button>
				</div>

				<div id="borda" class="none">
					<h4>Escolha a Borda</h4>	

					<div class="custom-control custom-radio">
						  <input type="radio" id="sim" name="borda" class="custom-control-input" 
						  onchange="pedido.escolhaRadios(event)" value="Sim">
						  <label class="custom-control-label" for="sim">Sim</label>
					</div>
					<div class="custom-control custom-radio">
						  <input type="radio" id="nao" name="borda" class="custom-control-input" 
						  onchange="pedido.escolhaRadios(event)" value="Não">
						  <label class="custom-control-label" for="nao">Não</label>
					</div>

					<div id="tem-borda">
						
					</div>

					<button type="button" class="btn btn-primary float-right" onclick="pedido.mostrarTotal()">
						Proximo
					</button>
				</div>

				<div class="modal" tabindex="-1" role="dialog" id="modal-total">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Total Pedido</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body" id="total">
							<!-- Conteudo da modal -->
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary">Enviar Pedido</button>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Salvar</button>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="js/Pedido.js"></script>
<script src="js/Service.js"></script>
<script>
	let pedido = new Pedido();
</script>



<?php
	include("includes/rodape.php")
?>