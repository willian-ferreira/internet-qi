<?php
    include("includes/cabecalho.php")
?>

	<div class="container">
		<div class="row mt-5">
		<div class="col-7">
			<img src="imagens/logo.png" alt="Logo">
		</div>

		<div class="col-5">
			<h2>Speed Pizza</h2>

			<p>
				Há mais de 15 anos, uma de nossas maiores especialidades é produzir pizzas em forno à lenha, assadas 
				pelo sistema tradicional, diretamente na pedra. Em nossas massas utilizamos apenas ingredientes frescos 
				e de procedência certificada. Para ir  até você, contamos com entregadores preparados e exclusivos. 
				Tudo para que a sua pizza chegue na sua mesa com mais agilidade, quentinha e saborosa.	
			</p>
		</div>
	</div>
	</div>


<?php
	include("includes/rodape.php")
?>