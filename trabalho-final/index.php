<?php
	include("includes/cabecalho.php")
?>

<div class="container">
	<img src="imagens/logo.png" class="logo">


	<!-- Carrossel -->


	<div id="carrossel-principal" class="carousel slide" data-ride="carousel">
	  	<ol class="carousel-indicators">
		    <li data-target="#carrossel-principal" data-slide-to="0" class="active"></li>
		    <li data-target="#carrossel-principal" data-slide-to="1"></li>
		    <li data-target="#carrossel-principal" data-slide-to="2"></li>
	 	</ol>
		<div class="carousel-inner">
		    <div class="carousel-item active">
		      	<img class="d-block w-100" src="imagens/pizza.png" alt="First slide">
		      	<div class="carousel-caption d-none d-md-block">
				    <h5>Promoção da Semana</h5>
				    <p>Pizza de Calabresa por R$ 25,00 se comprar no balcão</p>
				</div>
    		</div>
		    <div class="carousel-item">
		      	<img class="d-block w-100" src="imagens/pizza2.png" alt="Second slide">
		    </div>
		    <div class="carousel-item">
		      	<img class="d-block w-100" src="imagens/pizza3.jpg" alt="Third slide">
		    </div>
		 </div>
		<a class="carousel-control-prev" href="#carrossel-principal" role="button" data-slide="prev">
		    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		</a>
	  	<a class="carousel-control-next" href="#carrossel-principal" role="button" data-slide="next">
		    <span class="carousel-control-next-icon" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
	  	</a>
	</div>


	<div class="row mt-2">
		<div class="col-12 col-sm-6">
			
			<div id="carrossel-esquerda" class="carousel slide" data-ride="carousel">
			  	<ol class="carousel-indicators">
				    <li data-target="#carrossel-esquerda" data-slide-to="0" class="active"></li>
				    <li data-target="#carrossel-esquerda" data-slide-to="1"></li>
				    <li data-target="#carrossel-esquerda" data-slide-to="2"></li>
			 	</ol>
				<div class="carousel-inner">
				    <div class="carousel-item active">
				      	<img class="d-block w-100" src="imagens/pizza4.jpg" alt="First slide">

		    		</div>
				    <div class="carousel-item">
				      	<img class="d-block w-100" src="imagens/pizza5.jpg" alt="Second slide">
				    </div>
				    <div class="carousel-item">
				      	<img class="d-block w-100" src="imagens/pizza6.jpeg" alt="Third slide">
				    </div>
				 </div>
				<a class="carousel-control-prev" href="#carrossel-esquerda" role="button" data-slide="prev">
				    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				</a>
			  	<a class="carousel-control-next" href="#carrossel-esquerda" role="button" data-slide="next">
				    <span class="carousel-control-next-icon" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
			  	</a>
			</div>
		</div>



		<div class="col-12 col-sm-6">
			<div id="carrossel-direita" class="carousel slide" data-ride="carousel">
			  	<ol class="carousel-indicators">
				    <li data-target="#carrossel-direita" data-slide-to="0" class="active"></li>
				    <li data-target="#carrossel-direita" data-slide-to="1"></li>
				    <li data-target="#carrossel-direita" data-slide-to="2"></li>
			 	</ol>
				<div class="carousel-inner">
				    <div class="carousel-item active">
				      	<img class="d-block w-100" src="imagens/pizza7.jpg" alt="First slide">
		    		</div>
				    <div class="carousel-item">
				      	<img class="d-block w-100" src="imagens/pizza8.jpg" alt="Second slide">
				    </div>
				 </div>
				<a class="carousel-control-prev" href="#carrossel-direita" role="button" data-slide="prev">
				    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				</a>
			  	<a class="carousel-control-next" href="#carrossel-direita" role="button" data-slide="next">
				    <span class="carousel-control-next-icon" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
			  	</a>
			</div>
		</div>
		
	</div>	



</div>


<?php
	include("includes/rodape.php")
?>