<?php
    include("includes/cabecalho.php")
?>


<div class="container">
    <form class="form-row" onsubmit="cadastro.cadastrar(this, event)">
        <div class="form-group col-12 col-sm-12">
            <label for="nome">Nome</label>
            <input type="text" class="form-control" id="nome" placeholder="Nome" required>
        </div>
        <div class="form-group col-12 col-sm-12">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" placeholder="Email" required>
        </div>
        <div class="form-group col-6 col-sm-3">
            <label for="cep">CEP</label>
            <input type="text" class="form-control" id="cep" placeholder="CEP" onblur="cadastro.pesquisarCep(this.value);" maxlength="9" required>
        </div>
        <div class="form-group col-6 col-sm-3">
            <label for="numero">Número</label>
            <input type="number" class="form-control" id="numero" placeholder="Número" required>
        </div>
        <div class="form-group col-6 col-sm-6">
            <label for="complemento">Complemento</label>
            <input type="text" class="form-control" id="complemento" placeholder="Complemento" required>
        </div>

        <div class="form-group col-6 col-sm-7">
            <label for="rua">Rua</label>
            <input type="text" class="form-control" id="rua" placeholder="Rua" required>
        </div>
        <div class="form-group col-12 col-sm-5">
            <label for="bairro">Bairro</label>
            <input type="text" class="form-control" id="bairro" placeholder="Bairro" required>
        </div>
        <div class="form-group col-8 col-sm-10">
            <label for="cidade">Cidade</label>
            <input type="text" class="form-control" id="cidade" placeholder="Cidade" required>
        </div>

        <div class="form-group col-4 col-sm-2">
            <label for="uf">UF</label>
            <input type="text" class="form-control" id="uf" placeholder="UF" required>
        </div>
        <div class="form-group col-12 col-sm-12">
            <label for="mensagem">Mensagem</label>
            <textarea class="form-control" id="mensagem" placeholder="Deixe aqui a sua mensagem" rows="2"></textarea>
        </div>
        <div class="form-group col-12">
            <button class="btn btn-success float-right" type="submit">
                Cadastrar
            </button>
        </div>
        
    </form>
</div>




<script src="js/Cadastro.js"></script>
<script>
    let cadastro = new Cadastro();
</script>

<?php
    include("includes/rodape.php")
?>