class Pedido {

	constructor() { 
		this.ultimo;
		this.sabores = [];
		this.massa;
		this.tamanho;
		this.borda;
		this.valor = 0;
	}

	alterarJanelaPedido(id) {
		
		if(this.ultimo) {
			this.ultimo.classList.add("none");
		}

		if(id == "#sabores") {
			this.buscar();
		}

		let div = document.querySelector(id); 

		div.classList.remove("none");

		this.ultimo = div;

	}

	buscar() {
		let service = new Service();
		let array = service.getSabores(); 
		let array2 = service.getSaboresDoces(); 
		let div = document.querySelector("#sabores")
		let c1 = `<h4>Escolha os Sabores</h4>
			<div class="row">
			<div class="col">
			<h5>Salgadas</h5>`;
		for(let item of array) {
			let c = `
				<div class="custom-control custom-checkbox">
				  	<input type="checkbox" class="custom-control-input" id="${item.id}" onchange="pedido.escolherSabores('${item.nome}')">
				  	<label class="custom-control-label" for="${item.id}">${item.nome}</label>
				</div>
			`
			c1 += c;
		}
		c1 += `</div>
		<div class="col">
		<h5>Doces</h5>`
		for (let item of array2) {
			let c = `
				<div class="custom-control custom-checkbox">
				  	<input type="checkbox" class="custom-control-input" id="${item.id}" onchange="pedido.escolherSabores('${item.nome}')">
				  	<label class="custom-control-label" for="${item.id}">${item.nome}</label>
				</div>
			`
			c1 += c;
		}

		c1 += `
			</div>
			</div>
			<button type="button" class="btn btn-primary float-right" onclick="pedido.proximaTela('#tipo-massa', '#sabores')">
				Proximo
			</button>
			`

		div.innerHTML = c1;
	}


	escolherSabores(sabor){		
		if(this.sabores.includes(sabor)) {
			this.sabores = this.sabores.filter(item => item != sabor);
		} else {
			this.sabores.push(sabor);
		}
	}

	proximaTela(proxima, atual) {
		if ((!this.tamanho && proxima == "#sabores")
			|| this.sabores.length == 0 && proxima == "#tipo-massa"){
			alert("Preencha o dados Exigido");
			
		} else {
			if (this.tamanho == "Pequena" && this.sabores.length >= 2) {
				alert("Escolha apenas um sabor");
			} else {
				let proximaTela = document.querySelector(proxima);
				let telaAtual = document.querySelector(atual);
				proximaTela.classList.remove("none");
				telaAtual.classList.add("none");
			}	
		}
		if (proxima == "#sabores") {
			this.buscar();
		}
	}


	escolhaRadios(event) {
		if(event.target.name == "tamanho") {
			this.tamanho = event.target.value;
		} else if (event.target.name == "borda") {
			if (event.target.value ==  "Sim") {
				this.temBorda();
			} else {
				this.borda = "Nenhuma";
			}
		} else if (event.target.name == "tipo-borda") {
			this.borda = event.target.value;
		} else {
			this.massa = event.target.value;
		}
	}

	mostrarTotal() {
		this.calcularPreco();
		$("#modal-total").modal('show');
		let total = document.querySelector("#total");
		let html = `
			<h4> Total </h4>
			<p> Tamanho escolhido: ${this.tamanho} </p>
			<p> Sabores: `
		for (let sabor of this.sabores) {
			html += `${sabor}, `
		}

		html += `</p>
			<p>Tipo de Massa ${this.massa}</p>
			<p>Borda ${this.borda}</p>
			<p>Total: R$ ${this.preco},00</p>
		`;

		total.innerHTML = html;
	}


	temBorda() {
		let html = `
			<h4>Qual a borda?</h4>
			<div class="custom-control custom-radio">
					<input type="radio" id="catupiry" name="tipo-borda" class="custom-control-input" 
					onchange="pedido.escolhaRadios(event)" value="Catupiry">
					<label class="custom-control-label" for="catupiry" >Catupiry</label>
			</div>
			<div class="custom-control custom-radio">
					<input type="radio" id="cheedar" name="tipo-borda" class="custom-control-input" value="Média"
					onchange="pedido.escolhaRadios(event)">
					<label class="custom-control-label" for="cheedar">Cheedar</label>
			</div>
				
			`
			document.querySelector("#tem-borda").innerHTML = html;
	}

	calcularPreco() {
		if(this.tamanho == "Pequena") {
			this.preco = 20;
		} else if (this.tamanho == "Média") {
			this.preco = 25;
		} else {
			this.preco = 35;
		}

		if(this.borda != "Nenhuma") {
			this.preco += 5;
		}
	}
}







































