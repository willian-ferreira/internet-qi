var dadosUsuario = {};
var informacoes = {};

function salvarDados(event) {
	event.preventDefault();
 	this.dadosUsuario.nome = event.target[0].value;
 	this.dadosUsuario.sobre = event.target[1].value;
 	this.dadosUsuario.atuacao = event.target[2].value;
 	this.dadosUsuario.formacao = event.target[3].value;

 	let div = document.querySelector("#dados");

 	let html = `
 		<h4>Nome</h4>
 		<p class='nome'>${this.dadosUsuario.nome}</p>
 		<h4>Sobre</h4>
 		<p>${this.dadosUsuario.sobre}</p>
 		<h4>Área de Atuacão</h4>
 		<p>${this.dadosUsuario.atuacao}</p>
 		<h4>Formação</h4>
 		<p>${this.dadosUsuario.formacao}</p>
 	`

 	div.innerHTML = html;
}

function escolherFonte(element) {
	informacoes.fonteFamily = element.value;
}

function escolherCorFundo(element) {
	informacoes.backgroundColor = element.value;
}

function escolherTamNome(element) {
	informacoes.tamanhoNome = element.value;
}


function teste() {

	let div = document.querySelector("#dados");
	let p = document.querySelector(".nome");

	div.style.fontFamily = this.informacoes.fonteFamily;
	div.style.backgroundColor = this.informacoes.backgroundColor;
	p.style.fontSize = this.informacoes.tamanhoNome;

}

